package main

import (
	"trainingrest/app/config"
	"trainingrest/app/controller"
	"trainingrest/app/models"

	"github.com/gin-gonic/gin"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	// koneksi
	db := config.Connect()
	strDB := controller.StrDB{DB: db}
	// migration
	models.Migrations(db)

	router := gin.Default()
	v1 := router.Group("/api/v1")
	{
		// office
		v1.POST("/office", strDB.CreateOffice)
		v1.DELETE("/office/:id", strDB.DeleteOffice)
		v1.PUT("/office/:id", strDB.UpdateOffice)
		v1.GET("/office/", strDB.GetOffice)
		v1.GET("/office/:id/show", strDB.GetOneOffice)
		v1.GET("/office-search", strDB.GetSearchOffice)

		// user
		v1.POST("/user", strDB.CreateUser)
		v1.DELETE("/user/:id", strDB.DeleteUser)
		v1.PUT("/user/:id", strDB.UpdateUser)
		v1.GET("/user/", strDB.GetUser)
		v1.GET("/user/:id/show", strDB.GetOneUser)
		v1.GET("/user-search", strDB.GetSearchUser)

		// todo
		v1.POST("/todo", strDB.CreateTodos)
		v1.DELETE("/todo/:id", strDB.DeleteTodos)
		v1.PUT("/todo/:id", strDB.UpdateTodos)
		v1.GET("/todo/", strDB.GetTodos)
		v1.GET("/todo/:id/show", strDB.GetOneTodos)
		v1.GET("/todo-search", strDB.GetSearchTodos)
	}
	router.Run(":8080")
}
