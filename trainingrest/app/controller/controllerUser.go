package controller

import (
	"fmt"
	"net/http"
	"trainingrest/app/models"

	"github.com/gin-gonic/gin"
)

// CreateUser comment.
func (strDB *StrDB) CreateUser(c *gin.Context) {
	var (
		user   models.User
		result gin.H
	)

	err := c.Bind(&user)
	if err != nil {
		fmt.Println("tidak ada data")
	}
	strDB.DB.Create(&user)
	result = gin.H{
		"result": user,
	}
	c.JSON(http.StatusOK, result)
}

// DeleteUser comment.
func (strDB *StrDB) DeleteUser(c *gin.Context) {
	var (
		user   models.User
		result gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&user, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	fmt.Println(user)
	err = strDB.DB.Delete(&user).Error
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
	} else {
		result = gin.H{
			"result": "Data deleted successfully",
		}

	}

	c.JSON(http.StatusOK, result)
}

// UpdateUser comment.
func (strDB *StrDB) UpdateUser(c *gin.Context) {
	var (
		user    models.User
		newUser models.User
		result  gin.H
	)

	id := c.Param("id")
	err := c.Bind(&newUser)

	err = strDB.DB.First(&user, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}

	err = strDB.DB.Model(&user).Updates(newUser).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "update succesful",
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetUser comment.
func (strDB *StrDB) GetUser(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	strDB.DB.Find(&user)
	if len(user) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = responseAPI(user, len(user))
	}
	c.JSON(http.StatusOK, result)
}

// GetOneUser comment.
func (strDB *StrDB) GetOneUser(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	id := c.Param("id")
	strDB.DB.First(&user, id)
	if len(user) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = gin.H{
			"result": user,
		}
	}

	c.JSON(http.StatusOK, result)
}

// GetSearchUser comment.
func (strDB *StrDB) GetSearchUser(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	name := c.DefaultQuery("full_name", "")
	email := c.DefaultQuery("email", "")
	office := c.DefaultQuery("office_id", "")
	strDB.DB.Where("full_name LIKE ? OR email LIKE ? OR office_id LIKE ?", name, email, office).Find(&user)
	if len(user) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = responseAPI(user, len(user))
	}

	c.JSON(http.StatusOK, result)
}
